import socketIOClient from 'helpers/socketIOClient';
import { useEffect, useRef } from 'react';
import useAuth from '../auth/useAuth';

function useInitSocketIO() {
  const { auth } = useAuth();
  const ioClient = useRef<SocketIOClient.Socket>();
  useEffect(() => {
    ioClient.current && ioClient.current.disconnect();
    ioClient.current = socketIOClient('abc');
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ioClient.current.on('message', (data: any) => {
      console.log(data);
    });
    return () => {
      ioClient.current && ioClient.current.disconnect();
    };
  }, [auth]);
}

export default useInitSocketIO;
