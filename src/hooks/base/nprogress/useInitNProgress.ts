/* eslint-disable @typescript-eslint/no-empty-function */
import { Router } from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import useTheme from '../theme/useTheme';
import { changeNProgress } from './reducer';

function useInitNProgress() {
  const { theme } = useTheme();
  const dispatch = useDispatch();
  useEffect(() => {
    NProgress.configure({
      showSpinner: false,
      template: `<div class="bar" role="bar" 
                      style="background: ${theme.variables['@primary-color'] || '#1890ff'}">
                      <div class="peg" 
                        style="box-shadow: 0 0 10px ${
                          theme.variables['@primary-color'] || '#1890ff'
                        }, 0 0 5px ${theme.variables['@primary-color'] || '#1890ff'};">
                      </div>
                    </div>
                    <div class="spinner" role="spinner" style="">
                      <div 
                        class="spinner-icon" 
                        style="border-top-color: ${
                          theme.variables['@primary-color'] || '#1890ff'
                        }; border-left-color: ${theme.variables['@primary-color'] || '#1890ff'};">
                      </div>
                    </div>`
    });
    Router.events.on('routeChangeStart', () => {
      const actionChangeNProgress = changeNProgress({ status: 'start', loading: true });
      dispatch(actionChangeNProgress);
      NProgress.start();
    });
    Router.events.on('routeChangeComplete', () => {
      const actionChangeNProgress = changeNProgress({ status: 'complete', loading: false });
      dispatch(actionChangeNProgress);
      NProgress.done();
    });
    Router.events.on('routeChangeError', () => {
      const actionChangeNProgress = changeNProgress({ status: 'error', loading: false });
      dispatch(actionChangeNProgress);
      NProgress.done();
    });
    return () => {
      Router.events.off('routeChangeStart', () => {});
      Router.events.off('routeChangeComplete', () => {});
      Router.events.off('routeChangeError', () => {});
    };
  }, []);
}

export default useInitNProgress;
