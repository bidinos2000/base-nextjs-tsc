import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'pages/store';
import { changeLocale } from './reducer';
import { locales } from './reducer';

export type LocaleName = keyof typeof locales;

const langs: {
  value: LocaleName;
  label: string;
}[] = [];

for (const [key, value] of Object.entries(locales)) {
  langs.push({ value: key as LocaleName, label: value.name });
}

function useLocale() {
  const locale = useSelector((state: RootState) => state.locale);
  const dispatch = useDispatch();

  const setLocale = (newLocale: LocaleName) => {
    const actionChangeLocale = changeLocale(locales[newLocale].locale);
    dispatch(actionChangeLocale);
  };

  return { locale, langs, setLocale };
}

export default useLocale;
