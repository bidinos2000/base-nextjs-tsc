import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import moment from 'moment';
import { i18n } from 'pages/i18n';
import { useEffect } from 'react';
import useLocale from './useLocale';

function useInitLocale() {
  const { locale } = useLocale();
  useEffect(() => {
    moment.locale(locale.locale);
    i18n.changeLanguage(locale.locale);
    window.document.body.dataset.locale = locale.locale;
    localStorageHelper.setObject(KeyStorage.LOCALE, {
      ...locale
    });
  }, [locale]);
}

export default useInitLocale;
