import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Locale } from 'antd/es/locale-provider';
import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import _ from 'lodash';
import en from 'antd/lib/locale/en_US';
import vi from 'antd/lib/locale/vi_VN';
export const locales = {
  vi: { locale: vi, name: 'Tiếng Việt' },
  en: { locale: en, name: 'English' }
};

const defaultLocaleName = process.env.REACT_APP_LOCALE_DEFAULT || 'vi';

const initialState: Locale =
  localStorageHelper.getObject(KeyStorage.LOCALE, null) || locales[defaultLocaleName].locale;

const locale = createSlice({
  name: 'locale',
  initialState: initialState,
  reducers: {
    changeLocale: (state: Locale, action: PayloadAction<Locale>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = locale;
export const { changeLocale } = actions;
export default reducer;
