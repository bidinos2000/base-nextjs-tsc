import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import { router } from 'pages/i18n';
import paths from 'pages/paths';
import { useEffect } from 'react';
import useAuth from './useAuth';

interface Config {
  logged?: {
    redirect?: boolean;
  };
  noLogged?: {
    redirect?: boolean;
  };
}

function useInitAuth(config?: Config): void {
  const { auth } = useAuth();
  useEffect(() => {
    if (!auth || !auth.token) {
      localStorageHelper.remove(KeyStorage.AUTH);
      if (config?.noLogged?.redirect) {
        router.push(paths.auth.login);
      }
    } else {
      localStorageHelper.setObject(KeyStorage.AUTH, auth);
      if (config?.logged?.redirect) {
        router.push(paths.cms.index);
      }
    }
  }, [auth]);
}

export default useInitAuth;
