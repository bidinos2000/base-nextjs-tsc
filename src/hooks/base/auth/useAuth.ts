import { notification } from 'antd';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'pages/store';
import { changeAuth, LocalAuth } from './reducer';
import paths from 'pages/paths';
import { router } from 'pages/i18n';

export enum LOGIN_TYPE {
  DEFAULT = 0,
  FACEBOOK = 1,
  GOOGLE = 2,
  APPLE = 3
}

export interface DataLogin {
  username: string;
  password: string;
}

export interface Config {
  noti?: {
    success?: boolean;
    error?: boolean;
  };
  pushListData?: boolean;
}

function useAuth() {
  const auth = useSelector((state: RootState) => state.auth);
  const dispatch = useDispatch();
  const [logging, setLogging] = useState(false);

  const login = async (data: DataLogin, config?: Config) => {
    setLogging(true);
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, 1000);
    });
    config?.noti?.success && notification.success({ message: 'Successed' });
    const actionChangeLocale = changeAuth({ token: 'token' } as LocalAuth);
    dispatch(actionChangeLocale);
    router.push(paths.cms.index);
    setLogging(false);
    return false;
  };

  const logout = async () => {
    const actionChangeLocale = changeAuth({ token: null } as LocalAuth);
    dispatch(actionChangeLocale);
    return true;
  };

  return {
    auth,
    logging,
    login,
    logout
  };
}

export default useAuth;
