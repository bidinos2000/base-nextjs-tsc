import { changeTheme, ThemeState } from 'hooks/base/theme/reducer';
import { RootState } from 'pages/store';
import { useDispatch, useSelector } from 'react-redux';

export type DataTheme = {
  name?: ThemeState['name'];
  variables?: { [key: string]: string };
};

function useTheme() {
  const theme = useSelector((state: RootState) => state.theme);
  const dispatch = useDispatch();
  const setTheme = (
    dataTheme: {
      name?: ThemeState['name'];
      variables?: { [key: string]: string };
    } = {
      name: theme.name,
      variables: {}
    }
  ) => {
    const name = dataTheme.name;
    const variables = { ...theme.variables, ...dataTheme.variables };
    const actionChangeTheme = changeTheme({ name, variables });
    dispatch(actionChangeTheme);
  };

  return { theme, setTheme };
}

export default useTheme;
