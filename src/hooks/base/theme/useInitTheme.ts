import { default as localStorageHelper, KeyStorage } from 'helpers/localStorage';
import changeTheme from 'next-dynamic-antd-theme';
import { useEffect } from 'react';
import themes from 'themes';

import useTheme from './useTheme';

function useInitTheme(): void {
  const { theme } = useTheme();

  useEffect(() => {
    const newVariables = { ...theme.variables };
    const configTheme = {
      ...themes[theme.name],
      ...newVariables
    };
    changeTheme(configTheme);
    localStorageHelper.setObject(KeyStorage.THEME, theme);
    window.document.body.dataset.theme = theme.name;
  }, [theme]);
}

export default useInitTheme;
