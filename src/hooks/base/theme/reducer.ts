import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from 'helpers/localStorage';
import _ from 'lodash';
import themeVariables from 'themes/variables';

export interface ThemeState {
  name: 'default' | 'dark';
  variables: {
    [key: string]: string;
  };
}

const themeLocal = localStorageHelper.getObject(KeyStorage.THEME);

const initialState: ThemeState = {
  name: themeLocal.name || 'default',
  variables: Object.keys(themeLocal.variables || {}).length
    ? themeLocal.variables
    : { '@primary-color': themeVariables['@primary-color'] }
};

const theme = createSlice({
  name: 'theme',
  initialState: initialState,
  reducers: {
    changeTheme: (
      state: ThemeState,
      action: PayloadAction<{
        name?: ThemeState['name'];
        variables?: {
          [key: string]: string;
        };
      }>
    ) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = theme;
export const { changeTheme } = actions;
export default reducer;
