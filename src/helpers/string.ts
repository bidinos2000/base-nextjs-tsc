export enum defaultString {
  blank = ''
}

interface DataReplace {
  [key: string]: string | number;
}

export const replaceWithData = (
  str: string,
  data: DataReplace,
  keyStart = '{',
  keyEnd = '}'
): string => {
  for (const [key, value] of Object.entries(data)) {
    str = str.replace(new RegExp(`${keyStart}${key}${keyEnd}`, 'g'), `${value?.toString()}`);
  }
  return str;
};

export const getUrl = (str: string, params: DataReplace): string => {
  return replaceWithData(str, params, '/:', '');
};
