interface ObjectSortByKey {
  [key: string]: string | number | boolean | undefined | ObjectSortByKey | ObjectSortByKey[];
}
export const sortObjectByKey = (
  object: ObjectSortByKey,
  type: 'asc' | 'desc' = 'asc'
): ObjectSortByKey => {
  const newObject: ObjectSortByKey = {};
  const keys = Object.keys(object).sort();
  if (type === 'desc') {
    keys.reverse();
  }
  keys.forEach((key: string) => (newObject[key] = object[key]));
  return newObject;
};
