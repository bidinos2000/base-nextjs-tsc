/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import localStorage, { KeyStorage } from 'helpers/localStorage';
import statusCode from 'http-status-codes';
import queryString from 'query-string';

export interface Data {
  [key: string]: Data | number | string | undefined | null;
}
export interface IRequest {
  url: string;
  params: Data;
  data: Data;
}
export interface IResponse {
  statusCode: number;
  message: string;
  data: Data;
}

const instance = axios.create({
  baseURL: process.env.API_URL,
  timeout: 5000,
  paramsSerializer: (params) => queryString.stringify(params)
});

instance.interceptors.request.use((request: AxiosRequestConfig) => {
  return requestHandler(request);
});

instance.interceptors.response.use(
  (response: AxiosResponse) => {
    return successHandler(response);
  },
  (error: AxiosError) => {
    return errorHandler(error);
  }
);

const requestHandler = (request: AxiosRequestConfig) => {
  console.log(`>>> Request API: ${request.url}`);
  console.log(`  + Params:     `, request.params);
  console.log(`  + Data:       `, request.data);
  return request;
};

const successHandler = (response: AxiosResponse) => {
  console.log(`<<< Response API: ${response.config.url}`, response.data);
  return response.data;
};

const errorHandler = (error: AxiosError) => {
  throw error;
};

const getHeader = (customHeaders: any = {}): any => {
  const auth = localStorage.getObject(KeyStorage.AUTH);
  if (auth) {
    const token = auth.token;
    if (token) {
      return {
        ...customHeaders,
        Authorization: `Bearer ${token}`
      };
    }
  }

  return { ...customHeaders };
};

const axiosClient = {
  get: async <ReqType, ResType extends IResponse>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.get(url, { params, headers });
  },
  post: async <ReqType, ResType extends IResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.post(url, data, { headers });
  },
  put: async <ReqType, ResType extends IResponse>(
    url: string,
    data?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.put(url, data, { headers });
  },
  delete: async <ReqType, ResType extends IResponse>(
    url: string,
    params?: ReqType,
    customHeaders: any = {}
  ): Promise<ResType> => {
    const headers = getHeader(customHeaders);
    return instance.delete(url, { params, headers });
  },
  statusCode: statusCode
};
export default axiosClient;
