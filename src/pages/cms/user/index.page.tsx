import withPermission from 'pages/components/hocs/withPermission';
import IDatePicker from 'pages/components/input/IDatePicker';
import paths from 'pages/paths';
import MasterLayout from '../layouts/MasterLayout';

function IndexUserPage() {
  return (
    <MasterLayout>
      <IDatePicker />
    </MasterLayout>
  );
}
export default withPermission(IndexUserPage)({ permissions: [1, 2], redirect: paths.auth.login });
