import { Layout } from 'antd';
import BackTop from 'pages/components/base/BackTopCustom';

const { Content } = Layout;

interface Props {
  children?: JSX.Element | React.ReactNode | React.ReactNodeArray;
  toggleSider?: (status?: boolean) => void;
  hidden?: boolean;
}

function ContentLayout(props: Props) {
  const { hidden, children } = props;
  return (
    <Content>
      <div
        hidden={hidden}
        id="main-content"
        style={{
          overflowY: 'auto',
          height: '100%',
          position: 'relative',
          padding: '0 .5em'
        }}>
        <>
          {children}
          <BackTop
            style={{
              right: '.5em',
              bottom: '3em'
            }}
            targetId="main-content"
          />
        </>
      </div>
    </Content>
  );
}

export default ContentLayout;
