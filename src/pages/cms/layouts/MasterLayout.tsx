import { Layout } from 'antd';
import useInitAuth from 'hooks/base/auth/useInitAuth';
import useInitUiCms from 'hooks/base/ui-cms/useInitUiCms';
import dynamic from 'next/dynamic';
import ContentLayout from './content/ContentLayout';
import HeaderLayout from './header/HeaderLayout';
import Loading from 'pages/components/base/Loading';
import { RootState } from 'pages/store';
import { useSelector } from 'react-redux';

const SiderLayout = dynamic(() => import('./sider/SiderLayout'), { ssr: false });

interface Props {
  children?: JSX.Element | React.ReactNode;
}

function MasterLayout(props: Props) {
  useInitAuth({ noLogged: { redirect: true } });
  useInitUiCms();
  const nprogress = useSelector((state: RootState) => state.nprogress);
  const { children } = props;
  return (
    <Layout style={{ height: '100vh' }}>
      <SiderLayout />
      <Layout style={{ height: '100vh' }}>
        <HeaderLayout />
        <Loading loading={nprogress.status === 'start'} size="3em" />
        <ContentLayout hidden={nprogress.status === 'start'}>{children}</ContentLayout>
      </Layout>
    </Layout>
  );
}

export default MasterLayout;
