import { MenuUnfoldOutlined } from '@ant-design/icons';
import { Button, Col, Grid, Layout, Row, Space } from 'antd';
import useUiCms from 'hooks/base/ui-cms/useUiCms';
import Clock from 'pages/components/base/Clock';
import AvatarHeader from './AvatarHeader';
import NotificationHeader from './NotificationHeader';
import SettingHeader from './SettingHeader';

const { Header } = Layout;

function HeaderLayout() {
  const { uiCms, toggleSider } = useUiCms();
  const screens = Grid.useBreakpoint();
  return (
    <Header
      hidden={!uiCms.header.show}
      id="main-header"
      style={{
        padding: '0em',
        borderRadius: '.3em',
        margin: uiCms.header.margin,
        height: `calc(${uiCms.header.height} - ${uiCms.header.margin})`,
        lineHeight: `calc(${uiCms.header.height} - ${uiCms.header.margin})`
      }}>
      <Row gutter={[8, 0]} justify="space-between" align="middle">
        <Col>
          <Button
            hidden={uiCms.sider.show || screens.md}
            type="link"
            icon={<MenuUnfoldOutlined />}
            shape="circle"
            onClick={() => toggleSider()}
          />
        </Col>
        <Col>
          <div style={{ textAlign: 'right' }}>
            <Space>
              <Clock />
              <NotificationHeader />
              <AvatarHeader />
              <SettingHeader />
            </Space>
          </div>
        </Col>
      </Row>
    </Header>
  );
}

export default HeaderLayout;
