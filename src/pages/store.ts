import { configureStore, ConfigureStoreOptions } from '@reduxjs/toolkit';
import baseReducer from 'hooks/base/reducer';
import customReducer from 'reducers';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';

const rootReducer = {
  ...baseReducer,
  ...customReducer
};

const isClient = typeof window !== 'undefined';

const options: ConfigureStoreOptions = {
  reducer: rootReducer,
  devTools: true
};
if (isClient) {
  options.middleware = [
    createStateSyncMiddleware({
      whitelist: [
        'theme/changeTheme',
        'locale/changeLocale',
        'uiCms/changeUiCms',
        'auth/changeAuth'
      ]
    })
  ];
}

const store = configureStore(options);

if (isClient) {
  initMessageListener(store);
}

export type RootState = ReturnType<typeof store.getState>;
export default store;
