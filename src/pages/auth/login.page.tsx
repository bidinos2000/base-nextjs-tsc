import { Button, Card, Checkbox, Form, Input } from 'antd';
import useAuth, { DataLogin } from 'hooks/base/auth/useAuth';
import useInitAuth from 'hooks/base/auth/useInitAuth';
import ElementCenter from 'pages/components/base/ElementCenter';
import { RootState } from 'pages/store';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 }
};

const LoginPage = () => {
  useInitAuth({ logged: { redirect: true } });
  const { t } = useTranslation();
  const { logging, login } = useAuth();
  const { loading } = useSelector((state: RootState) => state.nprogress);

  const onFinish = (values: DataLogin) => {
    login({ username: values.username, password: values.password });
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <ElementCenter style={{ height: '100vh' }}>
      <Card headStyle={{ textAlign: 'center' }} title={t('login')}>
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}>
          <Form.Item label={t('form:labels.username')} name="username" rules={[{ required: true }]}>
            <Input />
          </Form.Item>

          <Form.Item label={t('form:labels.password')} name="password" rules={[{ required: true }]}>
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>{t('form:labels.remember_me')}</Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button loading={logging || loading} type="primary" htmlType="submit">
              {t('form:labels.login')}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </ElementCenter>
  );
};

export default LoginPage;
