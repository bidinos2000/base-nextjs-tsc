/* eslint-disable @typescript-eslint/no-empty-function */
import { ConfigProvider } from 'antd';
import { useTranslation } from './i18n';
import useInitLocale from 'hooks/base/locale/useInitLocale';
import useLocale from 'hooks/base/locale/useLocale';
import useInitNProgress from 'hooks/base/nprogress/useInitNProgress';
import useInitTheme from 'hooks/base/theme/useInitTheme';

interface Props {
  children: JSX.Element;
}

function Root(props: Props) {
  useInitTheme();
  useInitLocale();
  useInitNProgress();
  const { t } = useTranslation();
  const { children } = props;
  const { locale } = useLocale();
  return (
    <ConfigProvider
      locale={locale}
      form={{
        validateMessages: t('form:messages', { returnObjects: true })
      }}>
      {children}
    </ConfigProvider>
  );
}

export default Root;
