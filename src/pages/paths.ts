export default {
  auth: {
    login: '/auth/login'
  },
  cms: {
    index: '/cms/'
  },
  frontend: {
    index: '/'
  }
};
