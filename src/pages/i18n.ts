import 'moment/min/locales.min';
import NextI18Next from 'next-i18next';
import getConfig from 'next/config';
import path from 'path';

const { publicRuntimeConfig } = getConfig();
const defaultLang = process.env.LANGUAGE_DEFAULT || 'vi';
const nextI18Next = new NextI18Next({
  localeSubpaths: publicRuntimeConfig.localeSubpaths,
  localePath: path.resolve('./public/static/locales'),
  otherLanguages: [defaultLang, 'en'],
  defaultLanguage: defaultLang,
  ns: ['translations', 'messages', 'form', 'theme'],
  defaultNS: 'translations'
});
export const i18n = nextI18Next.i18n;
export const appWithTranslation = nextI18Next.appWithTranslation;
export const Link = nextI18Next.Link;
export const router = nextI18Next.Router;
export const useTranslation = nextI18Next.useTranslation;
export default nextI18Next;
