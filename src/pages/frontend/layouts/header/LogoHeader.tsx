import { GithubOutlined } from '@ant-design/icons';
import ElementCenter from 'pages/components/base/ElementCenter';

function LogoHeader() {
  return (
    <ElementCenter>
      <GithubOutlined style={{ fontSize: '3em', height: '100%' }} />
    </ElementCenter>
  );
}

export default LogoHeader;
