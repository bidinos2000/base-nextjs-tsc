import { Layout } from 'antd';
import { CSSProperties } from 'react';

const { Content } = Layout;

interface Props {
  style?: CSSProperties;
  children?: JSX.Element | React.ReactNode | React.ReactNodeArray;
  toggleSider?: (status?: boolean) => void;
}

function ContentLayout(props: Props) {
  const { style, children } = props;
  return (
    <Content>
      <div
        id="main-content"
        style={{
          overflowY: 'auto',
          height: '100%',
          position: 'relative',
          padding: '.5em',
          ...style
        }}>
        {children}
      </div>
    </Content>
  );
}

export default ContentLayout;
