import { Layout } from 'antd';
import dynamic from 'next/dynamic';
import BackTop from 'pages/components/base/BackTopCustom';
import HeaderLayout from 'pages/frontend/layouts/header/HeaderLayout';
import { useState } from 'react';
import ContentLayout from './content/ContentLayout';

const ChangeTheme = dynamic(() => import('pages/components/base/ChangeTheme'), { ssr: false });

const heightHeader = '4em';

interface Props {
  heightHeader?: number | string;
  children?: JSX.Element | React.ReactNode | React.ReactNodeArray;
}

function MasterLayout(props: Props) {
  const [fixedHeader] = useState(true);
  return (
    <div id="main-master">
      <Layout style={fixedHeader ? { height: '100vh' } : { minHeight: '100vh' }}>
        <HeaderLayout
          className="container"
          style={{
            height: heightHeader,
            position: fixedHeader ? 'absolute' : 'inherit',
            zIndex: 1,
            padding: 0
          }}
        />
        <ContentLayout
          style={{
            overflowY: fixedHeader ? 'auto' : 'inherit',
            height: fixedHeader ? `calc(100vh - ${fixedHeader ? heightHeader : '0px'})` : '100%',
            marginTop: fixedHeader ? heightHeader : 0
          }}>
          {props.children}
        </ContentLayout>
      </Layout>
      <BackTop
        style={{
          right: '.5em',
          bottom: '3em'
        }}
        targetId={fixedHeader ? 'main-content' : undefined}
      />
      <ChangeTheme />
    </div>
  );
}

export default MasterLayout;
