import { DatePicker } from 'antd';
import dynamic from 'next/dynamic';
import Container from 'pages/components/base/Container';
import MasterLayout from 'pages/frontend/layouts/MasterLayout';
import { Link, useTranslation } from 'pages/i18n';

const ChangeLanguage = dynamic(() => import('pages/components/base/ChangeLocale'), { ssr: false });

function IndexProductPage() {
  const { t } = useTranslation();
  return (
    <Container style={{ position: 'relative' }}>
      <MasterLayout>
        <Link href="/">{t('home')}</Link>
        <Link href="/admin">{t('admin')}</Link>
        <br />
        <DatePicker />
        <br />
        <ChangeLanguage />
      </MasterLayout>
    </Container>
  );
}

export default IndexProductPage;
