import 'antd/dist/antd.min.css';
import 'pages/global.scss';
import store from 'pages/store';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import Root from './root';
import { appWithTranslation } from 'pages/i18n';

function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Root>
        <Component {...pageProps} />
      </Root>
    </Provider>
  );
}

export default appWithTranslation(App);
