import { FormatPainterFilled } from '@ant-design/icons';
import { Button, Card, Dropdown, Menu } from 'antd';
import useTheme from 'hooks/base/theme/useTheme';
import { CSSProperties } from 'react';
import { ColorResult, HuePicker } from 'react-color';
import CirclePickerCustom from './CirclePickerCustom';

interface Props {
  style?: CSSProperties;
  type?: 'hue' | 'circle';
}

function ChangeTheme(props: Props) {
  const { style } = props;
  const { theme, setTheme } = useTheme();
  const menuTheme = () => {
    return (
      <Menu selectable={false} style={{ padding: 0 }}>
        <Menu.Item style={{ padding: 0 }}>
          <Card>
            {props?.type === undefined || props?.type === 'circle' ? (
              <CirclePickerCustom
                onChange={(color: ColorResult) => {
                  setTheme({ variables: { '@primary-color': color.hex } });
                }}
                color={theme.variables && theme.variables['@primary-color']}
              />
            ) : (
              ''
            )}
            {props?.type === 'hue' ? (
              <HuePicker
                onChange={(color: ColorResult) => {
                  setTheme({ variables: { '@primary-color': color.hex } });
                }}
                color={theme.variables && theme.variables['@primary-color']}
              />
            ) : (
              ''
            )}
          </Card>
        </Menu.Item>
      </Menu>
    );
  };

  return (
    <div
      style={{
        position: 'fixed',
        bottom: '1em',
        right: '1em',
        zIndex: 100,
        ...style
      }}>
      <Dropdown overlay={menuTheme()} trigger={['contextMenu']} placement="topRight">
        <Button
          onClick={() => {
            if (theme.name === 'dark') {
              setTheme({ name: 'default' });
            } else {
              setTheme({ name: 'dark' });
            }
          }}
          icon={
            <FormatPainterFilled
              style={{
                color: theme.variables && theme.variables['@primary-color']
              }}
            />
          }
          shape="circle"
          type="text"
        />
      </Dropdown>
    </div>
  );
}

export default ChangeTheme;
