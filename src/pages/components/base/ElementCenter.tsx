import { Col, Row } from 'antd';
import { RowProps } from 'antd/lib/row';

interface Props extends RowProps {
  children: JSX.Element;
}

const ElementCenter = (props: Props) => {
  const { children, ...reset } = props;
  return (
    <Row justify="center" align="middle" style={{ height: '100%' }} {...reset}>
      <Col style={{ display: 'flex' }}>{children}</Col>
    </Row>
  );
};

export default ElementCenter;
