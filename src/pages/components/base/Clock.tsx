import useClock from 'hooks/base/common/useClock';
import useLocale from 'hooks/base/locale/useLocale';

function Clock() {
  const { time } = useClock();
  const { locale } = useLocale();
  return <>{time.format(locale.DatePicker?.lang.dateTimeFormat)}</>;
}

export default Clock;
