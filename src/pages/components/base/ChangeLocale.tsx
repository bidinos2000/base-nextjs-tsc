import { TranslationOutlined } from '@ant-design/icons';
import { Select } from 'antd';
import useLocale from 'hooks/base/locale/useLocale';

function ChangeLocale() {
  const { locale, langs, setLocale } = useLocale();
  return (
    <Select
      options={langs}
      value={locale.locale}
      onChange={(value) => setLocale(value)}
      suffixIcon={<TranslationOutlined />}
    />
  );
}

export default ChangeLocale;
