import { VerticalAlignTopOutlined } from '@ant-design/icons';
import { BackTop, Button } from 'antd';
import { BackTopProps } from 'antd/lib/back-top';
import { ButtonShape, ButtonType } from 'antd/lib/button';

interface Props extends BackTopProps {
  targetId?: string;
  shape?: ButtonShape;
  icon?: JSX.Element;
  type?: ButtonType;
}

function BackTopCustom(props: Props) {
  const { targetId, style, type, shape, icon, ...reset } = props;
  return (
    <BackTop
      style={{
        right: '.5em',
        bottom: '.5em',
        ...(style || {})
      }}
      target={() => (targetId ? window.document.getElementById(targetId) : null) || window}
      {...reset}>
      <Button
        type={type || 'primary'}
        icon={icon || <VerticalAlignTopOutlined />}
        shape={shape || 'circle'}
      />
    </BackTop>
  );
}

export default BackTopCustom;
