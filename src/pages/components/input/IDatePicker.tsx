import { DatePicker } from 'antd';
import { PickerProps } from 'antd/es/date-picker/generatePicker';
import useLocale from 'hooks/base/locale/useLocale';
import { Moment } from 'moment';
import React from 'react';

function IDatePicker(props: PickerProps<Moment>) {
  const { locale } = useLocale();
  return <DatePicker format={locale.DatePicker?.lang.dateFormat} {...props} />;
}

export default IDatePicker;
